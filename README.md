# McConnect #

McConnect is a simple to use client for remote controlling your Minecraft server. It there for uses the built in [rcon](http://wiki.vg/Rcon) protocol. So no plugins or mods needed on your Minecraft server!

## Features ##

 * Remote control a Minecraft server
 * Use dialogs to send commands or
 * Type commands by hand
 * Non vanilla commands are supported
 * Define your own command dialogs

## Installation ##

1. Download the latest version of McConnect from the [downloads page](https://bitbucket.org/moonlight_200/mcconnect/downloads)
2. Unzip the file where ever you want

## Server Setup ##

To use McConnect with your Minecraft server you need to enable rcon first. Go to your `server.properties` and set

```
enable-rcon=true
```

You should also set a password for the rcon connection, so only authorized people can remote control your server:

```
rcon.password=<your password here>
```

The default port for rcon connections is `25575`, but if you want to or need to change it you can do that by changing the following to your needs.
```
rcon.port=25575
```

## Sources ##

 * [json-simple](https://code.google.com/p/json-simple/) to easily load the menus and dialogs
 * [MCRcon](https://github.com/barneygale/MCRcon) The rcon protocol implementation is based on [barneygale](https://github.com/barneygale)'s python implementation