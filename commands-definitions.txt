The commands file holds an json object with a single item called "menus".
The menu item is an array of objects, that represent the menus.

Menu:
	- "name": The displayed name of the menu.
	- "hasToConnect": Boolean, whether there has to bee a connection for this menu to be active or not.
	- "items": an array of items that are in the menu

Menu item:
	- "type": has to be one of "item", "separator" or "menu"
	- "name": The displayed name of the menu item.
	- "mnemonic": the character for keyboard navigation.
	- "description": A brief description of the command. (Only used, when a dialog is shown)
	- "command": The command to send, parameters can be set in {}
	- "parameters": A list of parameters.

Parameter:
	- The name is used in the "command" section.
	- "type": one of "text", "player", "coordinates", "block", "item", "effect", "achievement", "select", "integer"
	- "optional": Boolean, whether the parameter is optional or not
	- "description": So the user knows what to enter