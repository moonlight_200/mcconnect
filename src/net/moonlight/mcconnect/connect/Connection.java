/*
 * Copyright (c) 2015 Felix Beil
 * 
 * This file is part of McConnect.
 * 
 * McConnect is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * McConnect is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with McConnect. If not, see <http://www.gnu.org/licenses/>.
 */
package net.moonlight.mcconnect.connect;

import java.io.IOException;
import java.net.UnknownHostException;

/**
 * The connection interface used for every protocol.
 */
public interface Connection
{
	/**
	 * Starts a connection to a Minecraft server.
	 * @param host The host name or IP of the server.
	 * @param port The port to connect to.
	 * @throws UnknownHostException If the IP address of the host could not be determined.
	 * @throws IOException If an I/O error occurs when creating the socket.
	 */
	public void connect(String host, int port) throws UnknownHostException, IOException;

	/**
	 * Disconnects form the Minecraft server.
	 */
	public void disconnect();

	/**
	 * Checks, whether there is an active connection to a server.
	 * @return <i>True</i>, if currently connected to a server.
	 */
	public boolean isConnected();

	/**
	 * Sends a login packet to the server.
	 * @param usr The user name to login with.
	 * @param pw The password to login with.
	 * @return Whether the login was successful.
	 * @throws IOException If an I/O error occurs.
	 */
	public boolean login(String usr, String pw) throws IOException;

	/**
	 * Sends a command to the Minecraft server.
	 * @param cmd
	 */
	public String sendCommand(String cmd) throws IOException;
	
	public String getName();
}
