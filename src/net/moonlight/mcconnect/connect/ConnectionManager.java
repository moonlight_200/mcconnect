package net.moonlight.mcconnect.connect;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public class ConnectionManager
{
	private ArrayList<Connection> connTypes = new ArrayList<>();
	private Connection active = null;

	public boolean register(Connection connection) throws IllegalConnectionException
	{
		if(connTypes.contains(connection))
		{
			return false;
		}
		for(Iterator<Connection> iterator = connTypes.iterator(); iterator.hasNext();)
		{
			if(((Connection) iterator.next()).getName().equals(connection))
			{
				throw new IllegalConnectionException("Name already used.");
			}

		}
		connTypes.add(connection);
		if(connTypes.size() == 1)
		{
			active = connection;
		}
		return true;
	}

	public boolean remove(Connection connection)
	{
		if(connTypes.contains(connection))
		{
			return false;
		}
		connTypes.add(connection);
		return true;
	}

	public Connection[] getAviableConnections()
	{
		return connTypes.toArray(new Connection[0]);
	}

	public Connection getActive()
	{
		return active;
	}

	public Connection getConnectionByName(final String connectionType)
	{
		for(Iterator<Connection> iterator = connTypes.iterator(); iterator.hasNext();)
		{
			Connection connection = (Connection) iterator.next();
			if(connection.getName().equalsIgnoreCase(connectionType))
			{
				return connection;
			}
		}
		return null;
	}

	public boolean connect(final String connectionType, String host, int port) throws IOException
	{
		if(active != null)
		{
			disconnect();
		}
		active = getConnectionByName(connectionType);
		active.connect(host, port);
		return true;
	}

	public boolean login(String user, String password) throws IllegalStateException, IOException
	{
		if(active == null)
		{
			throw new IllegalStateException("Not connected");
		}
		if(active.login(user, password))
		{
			return true;
		}
		return false;

	}

	public void disconnect()
	{
		if(active == null)
		{
			throw new IllegalStateException("Not connected");
		}
		active.disconnect();
		active = null;
	}

	public String sendCommand(String cmd) throws IOException
	{
		if(active == null)
		{
			throw new IllegalStateException("Not connected");
		}
		String result = active.sendCommand(cmd);
		return result;
	}
}
