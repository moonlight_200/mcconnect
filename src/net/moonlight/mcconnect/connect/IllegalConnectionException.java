package net.moonlight.mcconnect.connect;

public class IllegalConnectionException extends Exception
{
	public IllegalConnectionException(String string)
	{
		super(string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
