/*
 * Copyright (c) 2015 Felix Beil
 * 
 * This file is part of McConnect.
 * 
 * McConnect is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * McConnect is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with McConnect. If not, see <http://www.gnu.org/licenses/>.
 */
package net.moonlight.mcconnect.connect;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.CharBuffer;
import java.nio.charset.Charset;

import net.moonlight.mcconnect.main.MCConnect;

/**
 *
 */
public class Rcon implements Connection
{
	/**
	 * The connection to the server.
	 */
	private Socket socket;

	/**
	 * The id of the currently sent request.
	 */
	private int requestID;

	@Override
	public void connect(String host, int port) throws UnknownHostException, IOException
	{
		disconnect();
		requestID = 0;
		socket = new Socket(host, port);
	}

	@Override
	public void disconnect()
	{
		if(socket != null)
		{
			try
			{
				socket.close();
			}
			catch(IOException exception)
			{
				exception.printStackTrace();
			}
			finally
			{
				socket = null;
			}
			MCConnect.logGui("Disconnected");
		}
	}

	@Override
	public boolean isConnected()
	{
		return socket != null;
	}

	@Override
	public boolean login(String usr, String pw) throws IOException
	{
		try
		{
			send(3, pw);
		}
		catch(IllegalAccessException exception)
		{
			return false;
		}
		return true;
	}
	
	public int getRequestID()
	{
		return requestID;
	}
	
	public Socket getSocket()
	{
		return socket;
	}

	/**
	 * Reads a little endian encoded integer.
	 * @param data The bytes to read the integer from.
	 * @param offset The offset in the data.
	 * @return An integer read from the data bytes.
	 */
	private int readInt(byte[] data, final int offset)
	{
		ByteBuffer byteBuffer = ByteBuffer.allocate(4);
		byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
		byteBuffer.put(data, offset, 4);
		byteBuffer.flip();
		return byteBuffer.getInt();
	}

	private String send(int pkType, String pkData) throws IOException, IllegalAccessException
	{
		if(socket == null)
		{
			throw new RuntimeException("Must connect before sending data!");
		}

		// Prepare headers
		ByteBuffer byteBuffer = ByteBuffer.allocate(8);
		byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
		byteBuffer.putInt(requestID); // RequestID
		byteBuffer.putInt(pkType); // Packet type

		byteBuffer.flip();
		byte[] header = byteBuffer.array();

		// Encode data
		byteBuffer = Charset.forName("UTF-8").encode(pkData);
		byteBuffer.flip();
		byte[] dataEncoded = byteBuffer.array();

		// Add padding (one null byte is added by the encode function)
		byteBuffer = ByteBuffer.allocate(dataEncoded.length + 1);
		byteBuffer.put(dataEncoded);
		byteBuffer.put((byte) 0); // Padding

		byteBuffer.flip();
		byte[] dataOut = byteBuffer.array();

		// Prepare packet length
		byteBuffer = ByteBuffer.allocate(4);
		byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
		byteBuffer.putInt(header.length + dataOut.length + 1);

		byteBuffer.flip();
		byte[] lenght = byteBuffer.array();

		// Build packet
		byteBuffer = ByteBuffer.allocate(5 + header.length + dataOut.length);
		byteBuffer.put(lenght);
		byteBuffer.put(header);
		byteBuffer.put(dataOut);
		byteBuffer.put((byte) 0);
		byteBuffer.flip();

		// Send packet
		OutputStream outputStream = socket.getOutputStream();
		outputStream.write(byteBuffer.array());
		outputStream.flush();

		// Wait for response
		InputStream inputStream = socket.getInputStream();
		while(inputStream.available() == 0)
			;

		// Read data
		String dataIn = "";
		while(inputStream.available() > 0)
		{
			// Read packet length
			byte[] inData = new byte[4];
			int bytesRead = socket.getInputStream().read(inData);
			if(bytesRead != 4)
			{
				throw new IOException("Could not read packet length!");
			}

			// Parse packet length
			int pkLength = readInt(inData, 0);

			// Read rest of packet
			inData = new byte[pkLength];
			bytesRead = socket.getInputStream().read(inData);
			if(bytesRead != pkLength)
			{
				throw new IOException("Could not read data!");
			}

			// Read header
			int inRequestID = readInt(inData, 0);
			int inPkType = readInt(inData, 4);

			// Check header and padding
			if(inRequestID == -1)
			{
				throw new IllegalAccessException("Login failed!");
			}
			if(inRequestID != requestID)
			{
				throw new IOException("Recived packet with wrong request id (id: " + inRequestID + ", should be: " + requestID + ")!");
			}
			if(inPkType < 0 || inPkType > 3)
			{
				throw new IOException("Recived unknown packet type (" + inPkType + ")!");
			}
			if(inData[pkLength - 2] != 0 || inData[pkLength - 1] != 0)
			{
				throw new IOException("Incorrect padding!");
			}

			// Read payload
			byteBuffer = ByteBuffer.allocate(pkLength - 10);
			byteBuffer.put(inData, 8, pkLength - 10);
			byteBuffer.flip();
			CharBuffer charBuffer = Charset.forName("UTF-8").decode(byteBuffer);
			StringBuilder sb = new StringBuilder();
			while(charBuffer.hasRemaining())
			{
				sb.append(charBuffer.get());
			}
			dataIn += sb.toString();
		}

		requestID++;
		return dataIn;
	}

	@Override
	public String sendCommand(String cmd) throws IOException
	{
		try
		{
			return send(2, cmd);
		}
		catch(IllegalAccessException exception)
		{
			System.err.println("Well, that is weired? o.O");
			exception.printStackTrace();
		}
		return "If you see this, something went wrong ...";
	}

	@Override
	public String getName()
	{
		return "Rcon";
	}
}
