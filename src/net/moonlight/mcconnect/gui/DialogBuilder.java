/*
 * Copyright (c) 2015 Felix Beil
 * 
 * This file is part of McConnect.
 * 
 * McConnect is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * McConnect is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with McConnect. If not, see <http://www.gnu.org/licenses/>.
 */
package net.moonlight.mcconnect.gui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.util.ArrayList;

import javax.swing.WindowConstants;

import net.moonlight.mcconnect.gui.prefabs.CommandDialog;
import net.moonlight.mcconnect.gui.prefabs.PanelButtons;
import net.moonlight.mcconnect.gui.prefabs.PanelCoordinates;
import net.moonlight.mcconnect.gui.prefabs.PanelNumber;
import net.moonlight.mcconnect.gui.prefabs.PanelSelect;
import net.moonlight.mcconnect.gui.prefabs.PanelText;
import net.moonlight.mcconnect.gui.prefabs.ParameterPanel;

/**
 * Builds a dialog for a command.
 */
public class DialogBuilder
{
	private MainFrame parent;
	private String cmd;
	private String title;
	private ArrayList<ParameterPanel> components;

	public DialogBuilder()
	{
		parent = null;
		components = new ArrayList<ParameterPanel>();
	}

	/**
	 * Adds a parameter of type coordinates to the dialog.
	 * @param name The name of the parameter. Used to identify this parameter and build its value into the command.
	 * @param description A brief description or caption for this parameter, so the user knows what to enter.
	 * @param optional Whether the parameter is optional or not.
	 */
	public void addCoordinatesParameter(String name, String description, boolean optional)
	{
		components.add(new PanelCoordinates(name, description, optional));
	}

	/**
	 * Adds a parameter of type number to the dialog.
	 * @param name The name of the parameter. Used to identify this parameter and build its value into the command.
	 * @param description A brief description or caption for this parameter, so the user knows what to enter.
	 * @param min The smallest allowed value.
	 * @param max The biggest allowed value.
	 * @param step Which steps to use, when the user clicks the up or down arrows.
	 * @param optional Whether the parameter is optional or not.
	 */
	public void addNumberParameter(String name, String description, int min, int max, int step, boolean optional)
	{
		components.add(new PanelNumber(name, description, min, max, step, optional));
	}

	/**
	 * Adds a parameter of type select to the dialog.
	 * @param name The name of the parameter. Used to identify this parameter and build its value into the command.
	 * @param description A brief description or caption for this parameter, so the user knows what to enter.
	 * @param keys An array of all possible values that will be displayed to the user.
	 * @param values An array of all possible values that will be built into the command.
	 * @param optional Whether the parameter is optional or not.
	 */
	public void addSelectParameter(String name, String description, String[] keys, String[] values, boolean optional)
	{
		components.add(new PanelSelect(name, description, keys, values, optional));
	}

	/**
	 * Adds a parameter of type text to the dialog.
	 * @param name The name of the parameter. Used to identify this parameter and build its value into the command.
	 * @param description A brief description or caption for this parameter, so the user knows what to enter.
	 * @param optional Whether the parameter is optional or not.
	 */
	public void addTextParameter(String name, String description, boolean optional)
	{
		components.add(new PanelText(name, description, optional));
	}

	public CommandDialog buildDialog()
	{
		// Check necessary variables
		if(cmd == null)
		{
			throw new IllegalArgumentException("No command set!");
		}
		if(parent == null)
		{
			throw new IllegalArgumentException("No parent set!");
		}

		// Build dialog
		CommandDialog cmdDialog = new CommandDialog(parent, cmd);
		cmdDialog.setTitle(title);
		cmdDialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		cmdDialog.setModal(true);
		cmdDialog.setResizable(false);
		cmdDialog.setType(Window.Type.UTILITY);
		cmdDialog.setLayout(new GridBagLayout());
		cmdDialog.setMinimumSize(new Dimension(300, 100));

		// Add components
		for(int i = 0; i < components.size(); i++)
		{
			ParameterPanel pp = components.get(i);
			pp.clear();
			GridBagConstraints c = new GridBagConstraints();
			c.gridx = 0;
			c.gridy = i;
			c.weightx = 1.0f;
			c.weighty = 0.0f;
			c.fill = GridBagConstraints.HORIZONTAL;
			c.anchor = GridBagConstraints.CENTER;
			c.insets = new Insets(5, 5, 0, 5);
			cmdDialog.add(pp, c);
			cmdDialog.addParameter(pp);
		}

		// Add buttons
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = components.size();
		c.weightx = 1.0f;
		c.weighty = 0.0f;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.anchor = GridBagConstraints.CENTER;
		c.insets = new Insets(5, 5, 5, 5);
		cmdDialog.add(new PanelButtons(cmdDialog), c);

		cmdDialog.pack();
		cmdDialog.setLocationRelativeTo(parent);
		cmdDialog.setVisible(true);
		return cmdDialog;
	}

	/**
	 * Sets the command that should be executed using this dialog.
	 */
	public DialogBuilder setCommand(String cmd)
	{
		this.cmd = cmd;
		return this;
	}

	public DialogBuilder setParent(MainFrame parent)
	{
		this.parent = parent;
		return this;
	}

	public DialogBuilder setTitle(String title)
	{
		this.title = title;
		return this;
	}
}
