/*
 * Copyright (c) 2015 Felix Beil
 * 
 * This file is part of McConnect.
 * 
 * McConnect is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * McConnect is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with McConnect. If not, see <http://www.gnu.org/licenses/>.
 */
package net.moonlight.mcconnect.gui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.text.ParseException;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.text.MaskFormatter;

/**
 * The dialog that shows up, when the user connects to a server.
 */
public class DialogConnectTo extends JDialog
{
	private static final long serialVersionUID = 1L;
	private JTextField textFieldServer;
	private JFormattedTextField textFieldPort;
	private JPasswordField textFieldPassword;

	public DialogConnectTo(MainFrame parent)
	{
		this(parent, false);
		pack();
		setLocationRelativeTo(parent);
		setVisible(true);
	}

	private DialogConnectTo(MainFrame parent, boolean setVisible)
	{
		setTitle("Connect To ...");
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setModal(true);
		setLayout(new GridBagLayout());
		setPreferredSize(new Dimension(350, 130));
		setResizable(false);
		addComponents(parent);
		setVisible(setVisible);
	}

	public DialogConnectTo(MainFrame parent, String server, int port)
	{
		this(parent, false);
		textFieldServer.setText(server);
		textFieldPort.setText(String.valueOf(port));
		pack();
		textFieldPassword.requestFocusInWindow();
		setLocationRelativeTo(parent);
		setVisible(true);
	}

	/**
	 * Adds all visual components to the frame.
	 * @param parent The parent frame.
	 */
	private void addComponents(MainFrame parent)
	{
		GridBagConstraints c = new GridBagConstraints();

		JLabel labelServer = new JLabel("Server:");
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 1;
		c.anchor = GridBagConstraints.LINE_START;
		c.fill = GridBagConstraints.NONE;
		c.insets = new Insets(10, 10, 2, 2);
		c.weightx = 0.0f;
		c.weighty = 1.0f;
		add(labelServer, c);

		textFieldServer = new JTextField();
		c.gridx = 1;
		c.gridy = 0;
		c.gridwidth = 1;
		c.anchor = GridBagConstraints.CENTER;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(10, 2, 2, 2);
		c.weightx = 1.0f;
		c.weighty = 1.0f;
		add(textFieldServer, c);

		JLabel labelPort = new JLabel("Port:");
		c.gridx = 2;
		c.gridy = 0;
		c.gridwidth = 1;
		c.anchor = GridBagConstraints.LINE_START;
		c.fill = GridBagConstraints.NONE;
		c.insets = new Insets(10, 2, 2, 2);
		c.weightx = 0.0f;
		c.weighty = 0.0f;
		add(labelPort, c);

		try
		{
			textFieldPort = new JFormattedTextField(new MaskFormatter("#####"));
			textFieldPort.setMinimumSize(new Dimension(45, 20));
			// 25575 is the default port for minecraft rcon
			textFieldPort.setText("25575");
			c.gridx = 3;
			c.gridy = 0;
			c.gridwidth = 1;
			c.anchor = GridBagConstraints.CENTER;
			c.fill = GridBagConstraints.HORIZONTAL;
			c.insets = new Insets(10, 2, 2, 10);
			c.weightx = 0.0f;
			c.weighty = 0.0f;
			add(textFieldPort, c);
		}
		catch(ParseException exception)
		{
			exception.printStackTrace();
		}

		JLabel labelPassword = new JLabel("Password:");
		c.gridx = 0;
		c.gridy = 1;
		c.gridwidth = 1;
		c.anchor = GridBagConstraints.LINE_START;
		c.fill = GridBagConstraints.NONE;
		c.insets = new Insets(2, 10, 2, 2);
		c.weightx = 0.0f;
		c.weighty = 1.0f;
		add(labelPassword, c);

		textFieldPassword = new JPasswordField();
		c.gridx = 1;
		c.gridy = 1;
		c.gridwidth = 3;
		c.anchor = GridBagConstraints.CENTER;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(10, 2, 2, 10);
		c.weightx = 0.0f;
		c.weighty = 1.0f;
		add(textFieldPassword, c);

		Box box = Box.createHorizontalBox();

		JButton buttonConnect = new JButton("Connect");
		buttonConnect.addActionListener(event ->
		{
			dispose();
			parent.connect(textFieldServer.getText(), Integer.valueOf(textFieldPort.getText()), textFieldPassword.getPassword());
		});
		box.add(buttonConnect);

		JButton buttonCancel = new JButton("Cancel");
		buttonCancel.addActionListener(event ->
		{
			dispose();
		});
		box.add(buttonCancel);

		c.gridx = 0;
		c.gridy = 2;
		c.gridwidth = 4;
		c.anchor = GridBagConstraints.LINE_END;
		c.fill = GridBagConstraints.NONE;
		c.insets = new Insets(2, 10, 10, 10);
		c.weightx = 1.0f;
		c.weighty = 1.0f;
		add(box, c);

		getRootPane().setDefaultButton(buttonConnect);
	}
}
