/*
 * Copyright (c) 2015 Felix Beil
 * 
 * This file is part of McConnect.
 * 
 * McConnect is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * McConnect is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with McConnect. If not, see <http://www.gnu.org/licenses/>.
 */
package net.moonlight.mcconnect.gui.helper;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import net.moonlight.mcconnect.connect.Connection;
import net.moonlight.mcconnect.main.MCConnect;

/**
 * Listens for window events and disconnects from any server, if the window is about to close and connected to a server.
 */
public class WindowListener extends WindowAdapter
{
	private Connection connection;

	public WindowListener(Connection connection)
	{
		this.connection = connection;
	}

	@Override
	public void windowClosed(WindowEvent event)
	{
		connection.disconnect();
	}

	@Override
	public void windowClosing(WindowEvent event)
	{
		connection.disconnect();
	}
}
