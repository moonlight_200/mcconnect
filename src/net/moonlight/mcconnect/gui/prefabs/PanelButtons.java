/*
 * Copyright (c) 2015 Felix Beil
 * 
 * This file is part of McConnect.
 * 
 * McConnect is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * McConnect is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with McConnect. If not, see <http://www.gnu.org/licenses/>.
 */
package net.moonlight.mcconnect.gui.prefabs;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;

public class PanelButtons extends ParameterPanel
{
	private static final long serialVersionUID = 1L;

	public PanelButtons(CommandDialog commandDialog)
	{
		super(null, false);
		setLayout(new GridBagLayout());

		GridBagConstraints c = new GridBagConstraints();

		JButton buttonCancel = new JButton("Cancel");
		buttonCancel.addActionListener(event ->
		{
			commandDialog.onCancel();
		});
		c.gridx = 0;
		c.gridy = 0;
		c.weightx = 1.0f;
		c.weighty = 1.0f;
		c.fill = GridBagConstraints.NONE;
		c.anchor = GridBagConstraints.LINE_END;
		c.insets = new Insets(0, 0, 0, 5);
		add(buttonCancel, c);

		JButton buttonSend = new JButton("Send");
		buttonSend.addActionListener(event ->
		{
			commandDialog.onSend();
		});
		c.gridx = 1;
		c.gridy = 0;
		c.weightx = 0.0f;
		c.weighty = 1.0f;
		c.fill = GridBagConstraints.NONE;
		c.anchor = GridBagConstraints.CENTER;
		c.insets = new Insets(0, 0, 0, 0);
		add(buttonSend, c);

		setPreferredSize(new Dimension(290, buttonSend.getPreferredSize().height));
	}

	@Override
	public void clear()
	{

	}

	@Override
	public String getValue()
	{
		return null;
	}
}
