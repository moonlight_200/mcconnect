/*
 * Copyright (c) 2015 Felix Beil
 * 
 * This file is part of McConnect.
 * 
 * McConnect is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * McConnect is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with McConnect. If not, see <http://www.gnu.org/licenses/>.
 */
package net.moonlight.mcconnect.gui.prefabs;

import javax.swing.JPanel;

/**
 * A panel that holds all components for one parameter in the command.
 */
public abstract class ParameterPanel extends JPanel
{
	private static final long serialVersionUID = 1L;

	protected boolean isOptional;
	protected boolean isOptionalEnabled;

	public ParameterPanel(String name, boolean optional)
	{
		setName(name);
		isOptional = optional;
		isOptionalEnabled = false;
	}

	/**
	 * Clear all input fields.
	 */
	public abstract void clear();

	/**
	 * Builds a String that represents the contents of this parameter.
	 * @return The value of this parameter.
	 */
	public abstract String getValue();

	/**
	 * Whether the parameter is optional or not, if so it must not be entered a value.
	 * @return Whether the parameter is optional or not.
	 */
	public boolean isOptional()
	{
		return isOptional;
	}

	public boolean isOptionalEnabled()
	{
		return isOptionalEnabled;
	}
}
