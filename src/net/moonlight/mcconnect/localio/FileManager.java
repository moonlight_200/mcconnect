/*
 * Copyright (c) 2015 Felix Beil
 * 
 * This file is part of McConnect.
 * 
 * McConnect is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * McConnect is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with McConnect. If not, see <http://www.gnu.org/licenses/>.
 */
package net.moonlight.mcconnect.localio;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import net.moonlight.mcconnect.main.MCConnect;

/**
 * Holds the paths to all necessary files and checks them on start.
 */
public class FileManager
{
	/**
	 * The file that stores all Minecraft vanilla commands.
	 */
	public static final String DEFAULT_COMMADS = "commands.json";
	/**
	 * The directory that stores all plugins.
	 */
	public static final String PLUGINS_FOLDER = "plugins/";
	/**
	 * The main configuration file.
	 */
	public static final String CONFIG_FILE = "config.xml";

	private FileManager()
	{

	}

	/**
	 * Checks if all necessary files exist and either creates them or notifies the user.
	 */
	public static void checkFiles()
	{
		if(!new File(DEFAULT_COMMADS).exists())
		{
			MCConnect.logGui("Missing file '" + DEFAULT_COMMADS + "'!");
		}

		if(!new File(CONFIG_FILE).exists())
		{
			MCConnect.logGui("Missing file '" + CONFIG_FILE + "'!");
			SettingsManager.forceDefaults();
		}

		File pluginsFile = new File(PLUGINS_FOLDER);
		if(!pluginsFile.exists())
		{
			MCConnect.logGui("Missing directory '" + PLUGINS_FOLDER + "'!");
			if(!pluginsFile.mkdir())
			{
				MCConnect.logGui("Failed to create directory '" + PLUGINS_FOLDER + "'!");
			}
			pluginsFile.mkdir();
		}
	}

	public static boolean copy(File source, File dest)
	{
		if(!source.exists())
		{
			return false;
		}
		InputStream is = null;
		OutputStream os = null;
		try
		{
			try
			{
				is = new FileInputStream(source);
				os = new FileOutputStream(dest);
				byte[] buffer = new byte[1024];
				int length;
				while((length = is.read(buffer)) > 0)
				{
					os.write(buffer, 0, length);
				}
			}
			finally
			{
				is.close();
				os.close();
			}
		}
		catch(IOException e)
		{
			return false;
		}
		return true;

	}
}
