/*
 * Copyright (c) 2015 Felix Beil
 * 
 * This file is part of McConnect.
 * 
 * McConnect is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * McConnect is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with McConnect. If not, see <http://www.gnu.org/licenses/>.
 */
package net.moonlight.mcconnect.localio;

import java.awt.Font;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import net.moonlight.mcconnect.main.MCConnect;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Loads and saves all settings.
 */
public class SettingsManager
{
	private class TreeNode
	{
		private String name;
		private List<TreeNode> children;
		private String value;

		public TreeNode(String name)
		{
			this.name = name;
			children = new ArrayList<TreeNode>();
		}

		public TreeNode(String name, String value)
		{
			this.name = name;
			this.value = value;
		}

		/**
		 * Inserts the given property into the tree.
		 * @param nodeNames The name of all nodes.
		 * @param nameIndex The index of the name of the node to currently insert.
		 * @param value The value of the property.
		 */
		public void inset(String[] nodeNames, int nameIndex, String value)
		{
			String nodeName = nodeNames[nameIndex];
			if(nameIndex >= nodeNames.length - 1)
			{
				children.add(new TreeNode(nodeName, value));
				return;
			}

			for(int i = 0; i < children.size(); i++)
			{
				if(children.get(i).equals(nodeName))
				{
					children.get(i).inset(nodeNames, nameIndex + 1, value);
					return;
				}
			}

			TreeNode treeNode = new TreeNode(nodeName);
			children.add(treeNode);
			treeNode.inset(nodeNames, nameIndex + 1, value);
		}

		/**
		 * Appends all children to the element.
		 * @param doc The document to insert all nodes.
		 * @param parentElement The parent element.
		 */
		public void appendTo(Document doc, Element parentElement)
		{
			Element element = doc.createElement(name);
			if(children == null)
			{
				element.setTextContent(value);
			}
			else
			{
				for(int i = 0; i < children.size(); i++)
				{
					children.get(i).appendTo(doc, element);
				}
			}
			parentElement.appendChild(element);
		}

		@Override
		public boolean equals(Object obj)
		{
			if(obj instanceof String)
			{
				return name.equals(obj);
			}
			if(obj instanceof TreeNode)
			{
				TreeNode other = (TreeNode) obj;
				return name.equals(other.name) && (children == null ? other.children == null : children.equals(other.children)) && (value == null ? other.value == null : value.equals(other.value));
			}
			return false;
		}
	}

	private static boolean useDefault = false;

	private Properties properties;

	public SettingsManager()
	{
		properties = new Properties();
	}

	public String getStringProperty(String key)
	{
		return (String) properties.get(key);
	}

	public int getIntegerProperty(String key)
	{
		return Integer.parseInt(getStringProperty(key));
	}

	public void setStringProperty(String key, String value)
	{
		properties.put(key, value);
	}

	public void setIntegerProperty(String key, int value)
	{
		properties.put(key, String.valueOf(value));
	}

	/**
	 * Loads the configurations.
	 */
	public void loadConfig()
	{
		loadDefaultConfig();
		if(useDefault)
		{
			saveConfig();
			return;
		}

		File configFile = new File(FileManager.CONFIG_FILE);
		if(!configFile.exists())
		{
			return;
		}

		Document doc = null;
		try
		{
			doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new File(FileManager.CONFIG_FILE));
		}
		catch(IOException exception)
		{
			MCConnect.logGui("Failed to load configuration!");
			exception.printStackTrace();
			return;
		}
		catch(SAXException | ParserConfigurationException exception)
		{
			MCConnect.logGui("Failed to parse configuration file!");
			exception.printStackTrace();
			return;
		}

		// Check if root node exists
		if(!doc.hasChildNodes())
		{
			MCConnect.logGui("Configuration file is corrupted! (document is empty)");
			return;
		}

		// Check root node name
		Node rootNode = doc.getFirstChild();
		if(!"config".equals(rootNode.getNodeName().toLowerCase()))
		{
			MCConnect.logGui("Configuration file is corrupted! (root node is not 'config')");
			return;
		}

		loadNodes("", rootNode);
	}

	/**
	 * Loads the default values for all settings.
	 */
	public void loadDefaultConfig()
	{
		properties.put("general.logformat.time", "HH:mm");
		properties.put("general.logformat.font.name", "Monospaced");
		properties.put("general.logformat.font.size", String.valueOf(12));
		properties.put("general.logformat.font.style", String.valueOf(Font.PLAIN));
	}

	/**
	 * Loads all nodes to the property map.
	 * @param key The key of the parent node.
	 * @param node The node to load.
	 */
	private void loadNodes(String key, Node node)
	{
		// Build key for this node
		key += ".";
		key += node.getNodeName().toLowerCase();

		// If no children, load property
		if(!node.hasChildNodes())
		{
			properties.put(key, node.getTextContent());
			return;
		}

		// Load all child nodes
		NodeList nodeList = node.getChildNodes();
		for(int i = 0; i < nodeList.getLength(); i++)
		{
			loadNodes(key, nodeList.item(i));
		}
	}

	/**
	 * Saves the current configuration.
	 */
	public void saveConfig()
	{
		TreeNode rootNode = new TreeNode("config");
		for(Object obj : properties.keySet())
		{
			String key = null;
			try
			{
				key = (String) obj;
			}
			catch(ClassCastException exception)
			{
				exception.printStackTrace();
			}
			if(key != null)
			{
				rootNode.inset(key.split("\\."), 0, properties.getProperty(key));
			}
		}

		// Create new document
		Document doc;
		try
		{
			doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
		}
		catch(ParserConfigurationException exception)
		{
			exception.printStackTrace();
			return;
		}

		// Create root element
		Element rootElement = doc.createElement("config");

		// Append all child elements
		rootNode.appendTo(doc, rootElement);

		// Add root element to document
		doc.appendChild(rootElement.getFirstChild());

		// Create the file
		File configFile = new File(FileManager.CONFIG_FILE);
		if(!configFile.exists())
		{
			try
			{
				if(!configFile.createNewFile())
				{
					MCConnect.logGui("Could not create configuration file!");
					return;
				}
			}
			catch(IOException exception)
			{
				exception.printStackTrace();
				MCConnect.logGui("Could not create configuration file!");
				return;
			}
		}
		
		// Write the document to the file
		try
		{
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("indent-amount", "4");
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			transformer.setOutputProperty(OutputKeys.VERSION, "1.0");
			transformer.transform(new DOMSource(doc), new StreamResult(configFile));
		}
		catch(TransformerConfigurationException exception)
		{
			exception.printStackTrace();
		}
		catch(TransformerException exception)
		{
			exception.printStackTrace();
		}
		catch(TransformerFactoryConfigurationError error)
		{
			error.printStackTrace();
		}
	}

	/**
	 * Do not load the configuration from the file. Use the default values instead.
	 */
	public static void forceDefaults()
	{
		useDefault = true;
	}
}
