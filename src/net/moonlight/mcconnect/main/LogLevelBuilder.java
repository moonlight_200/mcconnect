/*
 * Copyright (c) 2015 Felix Beil
 * 
 * This file is part of McConnect.
 * 
 * McConnect is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * McConnect is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with McConnect. If not, see <http://www.gnu.org/licenses/>.
 */
package net.moonlight.mcconnect.main;

import java.util.logging.Level;

/**
 * Builds new log levels.
 */
public class LogLevelBuilder extends Level
{
	private static final long serialVersionUID = 1L;
	private static int levelValue = 10000;
	
	private LogLevelBuilder(String name)
	{
		super(name, levelValue++);
	}
	
	public static Level createLevel(String name)
	{
		return new LogLevelBuilder(name);
	}
}
