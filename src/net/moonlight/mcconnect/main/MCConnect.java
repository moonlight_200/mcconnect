/*
 * Copyright (c) 2015 Felix Beil
 * 
 * This file is part of McConnect.
 * 
 * McConnect is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * McConnect is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with McConnect. If not, see <http://www.gnu.org/licenses/>.
 */
package net.moonlight.mcconnect.main;

import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JTextArea;
import javax.swing.UIManager;

import net.moonlight.mcconnect.connect.ConnectionManager;
import net.moonlight.mcconnect.connect.IllegalConnectionException;
import net.moonlight.mcconnect.connect.Rcon;
import net.moonlight.mcconnect.gui.MainFrame;
import net.moonlight.mcconnect.gui.helper.Formatter;
import net.moonlight.mcconnect.gui.helper.TextAreaHandler;
import net.moonlight.mcconnect.localio.FileManager;
import net.moonlight.mcconnect.localio.SettingsManager;

/**
 * Handles the Minecraft rcon protocol.
 */
public final class MCConnect
{
	public static final Logger logger = Logger.getLogger("mcconnect");
	public static final Level LOG_LEVEL_GUI = LogLevelBuilder.createLevel("Gui");
	public static final Level LOG_LEVEL_SERVER = LogLevelBuilder.createLevel("Server");
	private static SettingsManager settingsManager;
	private static ConnectionManager connectionManager;
	
	public static ConnectionManager getConnectionManager()
	{
		return connectionManager;
	}

	/**
	 * Used for update checks.
	 * @return The non visible internal program version.
	 */
	public static int getInternalVersion()
	{
		return 21;
	}

	/**
	 * The visible program version.
	 * @return The visible program version.
	 */
	public static String getVersion()
	{
		return "0.8.1";
	}

	/**
	 * Log a gui related message.
	 * @param msg The message to log.
	 */
	public static void logGui(String msg)
	{
		logger.log(LOG_LEVEL_GUI, msg);
	}
	
	public static SettingsManager getSettingsManager()
	{
		return settingsManager;
	}

	/**
	 * Log a server related message.
	 * @param msg The message to log.
	 */
	public static void logServer(String msg)
	{
		logger.log(LOG_LEVEL_SERVER, msg);
	}

	public static void main(String[] args)
	{
		try
		{
			// Set LookAndFeel to system default
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}
		catch(Exception exception)
		{
			// or not ...
			exception.printStackTrace();
		}

		setupLogger();

		JTextArea textAreaOutput = new JTextArea();
		Handler handlerTexArea = new TextAreaHandler(textAreaOutput);
		handlerTexArea.setFormatter(new Formatter());
		logger.addHandler(handlerTexArea);
		
		settingsManager = new SettingsManager();
		FileManager.checkFiles();
		settingsManager.loadConfig();
		
		for(Handler handler : logger.getHandlers())
		{
			((Formatter) handler.getFormatter()).setDateFormat(settingsManager.getStringProperty("general.logformat.time"));
		}
		
		connectionManager = new ConnectionManager();
		try
		{
			connectionManager.register(new Rcon());
		}
		catch(IllegalConnectionException e)
		{
			e.printStackTrace();
		}
		
		
		new MainFrame(textAreaOutput);
		
		logGui("McConnect v" + getVersion());
	}
	/**
	 * Setup the logger to output to the console in correct formatting.
	 */
	private static void setupLogger()
	{
		logger.setUseParentHandlers(false);

		Handler handlerConsole = new ConsoleHandler();
		handlerConsole.setFormatter(new Formatter());
		logger.addHandler(handlerConsole);
	}
}
